// Translate Data models into codes

// Users Collection
{
	"_id": "user001",
	"firstName": "Prescilla",
	"lastName": "Lequin",
	"email" : "prescyaqoh@mail.com",
	"password": "Prescilla123",
	"isAdmin": true,
	"mobileNumber" : "123456789",
	"dateTimeRegistered": "2022-06-10"

}

{
	"_id": "user002",
	"firstName": "Jane",
	"lastName": "Doe",
	"email" : "jane@mail.com",
	"password": "janedoe",
	"isAdmin": false,
	"mobileNumber" : "123456789",
	"dateTimeRegistered": "2022-06-10"
	

}


// Orders Collection (Orders.json)
{
	"_id" : "orders001",
	"userId": "user002",//to be added
	"transactionDate":"2022-06-10",
	"Status": "Pending",
	"Total": 180,
	"dateTimeRegistered": "2022-06-10"
}

// Order Products Collection (OrderProducts.json)
{
	"_id": "orderPruducts001",
	"orderId": "orders001",//to be added
	"productId": "products001",//to be added
	"Quantity": 2,
	"price": 50,//to be added
	"subTotal": 100,//to be added
	"dateTimeCreated": "2022-06-10"
}

{
	"_id": "orderPruducts002",
	"orderId": "orders001",//to be added
	"productId": "products002",//to be added
	"Quantity": 1,
	"price": 80,//to be added
	"subTotal": 80,//to be added
	"dateTimeCreated": "2022-06-10"
}

// Products Collection (Products.json)
{
	"_id": "products001",
	"productName": "Shampoo",
	"description": "for the hair",
	"price": 50,
	"Stocks": 28,
	"isActive": true;
	"SKU":"Shower-123"
	"dateTimeCreated": "2022-06-10"
}
{
	"_id": "products002",
	"productName": "Diswashing",
	"description": "for the plates",
	"price": 80,
	"Stocks": 19,
	"isActive": true;
	"SKU":"Kitchen-123"
	"dateTimeCreated": "2022-06-10"
}

